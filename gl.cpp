#include <GL/glut.h>
#include <iostream>
#include "gl.h"

void timer(int)
{
	glutPostRedisplay();
	glutTimerFunc(1000.0/maxFrameRate, timer, 0);
}

void keyDown(unsigned char ch, int x, int y)
{
	switch ( tolower(ch) )
	{
	case 'q': // sx su
		movingL = 1;
		break;
	case 'a': // sx giù
		movingL = -1;
		break;
	case 'p': // dx su
		movingR = 1;
		break;
	case 'l': // dx giù
		movingR = -1;
		break;
	case ' ':
		if(!animating)
			animating = true;
		break;
	case 'r':
		die();
		break;
	}
}

void keyUp(unsigned char ch, int x, int y)
{
	switch ( tolower(ch) )
	{
	case 'q': // sx su
		if(movingL==1) movingL=0;
		break;
	case 'a': // sx giù
		if(movingL==-1) movingL=0;
		break;
	case 'p': // dx su
		if(movingR==1) movingR=0;
		break;
	case 'l': // dx giù
		if(movingR==-1) movingR=0;
		break;
	}
}

// Chiamata a ogni frame
void display()
{
	// Framerate
	dt = glutGet(GLUT_ELAPSED_TIME) - elapsedTime; // Durata del frame (ms)
	elapsedTime = glutGet(GLUT_ELAPSED_TIME);
	
	glClearColor(0, 0, 0, 0.0); // Colori della finestra quando viene chiamata
	glClear(GL_COLOR_BUFFER_BIT); // Boh
	
	// Perdita
	if ( abs(xPall) >= 1 && animating )
		die();
	
	if ( animating )
		updatePos();
		
	updateRectangles(_left, _right);
	drawBall(r,xPall,yPall);
	
	glutSwapBuffers();
}
