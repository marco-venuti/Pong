#include <iostream>
#include <cstdlib>
#include "gl.h"

void scoreAdd()
{
	score++; // Incrementa punteggio
	vxPall += 0.1; // Incrementa velocità
	vyPall += 0.1;
	cout << "Punteggio: " << score << endl;
	cout.flush();
}

void die()
{
	cout << "\nGAME OVER\nHai perso avendo totalizzato " << score << (score == 1 ? " punto" : " punti")
	     << "\n\nPremi BARRA SPAZIATRICE per ricominciare\n";
	cout.flush();	
	resetGame();
}

void resetGame()
{
	_left = _right = -RECT_H/2;
	movingL = movingR = 0;
	xPall = yPall = 0;

    srand(time(0));
    
    vxPall = (rand() % 2 ? 0.5 : -0.5);
    vyPall = (rand() % 2 ? 0.5 : -0.5);
    
    score = 0;
	//display();
	animating = false;
}
