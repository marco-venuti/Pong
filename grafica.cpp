#include <GL/glut.h>
#include <cmath>
#include "gl.h"
/*
#include <iostream>
using namespace std;
*/
double _left, _right; // Posizioni piattelli
int movingL, movingR; // Movimento piattelli
double r = 0.1, xPall, yPall, vxPall, vyPall; // Raggio, posizione e velocità pallina
bool toScatter = true;
int elapsedTime, dt, maxFrameRate = 60;
bool animating;
int score;

void updateRectangles(double __left, double __right)
{	
	drawRectangle(-0.8, __left,  -RECT_W, RECT_H);
	drawRectangle( 0.8, __right, RECT_W, RECT_H);	
}

void drawRectangle(double topLeftX, double topLeftY, double width, double height)
{
    glColor3f(0.408, 0.345, 0.141);
	glBegin(GL_POLYGON);
		glVertex3f(topLeftX, topLeftY, 0.0);
		glVertex3f(topLeftX+width, topLeftY, 0.0);
		glVertex3f(topLeftX+width, topLeftY+height, 0.0);
		glVertex3f(topLeftX, topLeftY+height, 0.0);
	glEnd();
}

void drawBall(double radius, double x, double y)
{
	int edges = 20;
    glColor3f(0.898, 0.796, 0.125);
	glBegin(GL_POLYGON);
		for (int i=0; i<edges; i++)
			glVertex3f( x + radius*cos( 2.0*PI/edges*i ), y + radius*sin( 2.0*PI/edges*i ), 0.0 );
	glEnd();
}

void updatePos()
{
	// Movimento dei piattelli
	if (movingL==1)
		_left += 1.0*dt/1000;
	else if (movingL==-1)
		_left -= 1.0*dt/1000;
	
	if (movingR==1)
		_right += 1.0*dt/1000;
	else if (movingR==-1)
		_right -= 1.0*dt/1000;
		
	// Condizioni di bordo piattelli
	if ( _left > 1 - RECT_H ) _left = 1 - RECT_H;
	else if ( _left < -1 ) _left = -1;
	
	if ( _right > 1 - RECT_H ) _right = 1 - RECT_H;
	else if ( _right < -1 ) _right = -1;
	
	// Moto pallina
	xPall += vxPall*dt/1000;
	yPall += vyPall*dt/1000;
	
	if (toScatter)
	{
		// Urto sulla parete
		if (   ( xPall <= -0.8 + r && xPall > -0.82 && yPall > _left  && yPall < _left  + RECT_H )
			|| ( xPall >=  0.8 - r && xPall <  0.82 && yPall > _right && yPall < _right + RECT_H ) )
		{
			vxPall = -vxPall;
			toScatter = false;
			scoreAdd();
		}
		// Urto sullo spigolo
		else if (( pow( xPall + 0.8, 2 ) + pow( yPall - _left, 2 ) < r*r )
		 || ( pow( xPall + 0.8, 2 ) + pow( yPall - _left - RECT_H, 2) < r*r)
		 || ( pow( xPall - 0.8, 2 ) + pow( yPall - _right, 2 ) < r*r )
		 || ( pow( xPall - 0.8, 2 ) + pow( yPall - _right - RECT_H, 2 ) < r*r ))
		{		
			// Theta è l'angolo che l'impulso trasferito (che va verso il centro della pallina) forma con l'asse x
			double sinTheta = (yPall - ( xPall < 0 ? _left : _right ) - ( yPall > ( xPall < 0 ? _left : _right ) + RECT_H/2 ? RECT_H : 0 ) )/r;
			double cosTheta = sqrt(1-sinTheta*sinTheta);
			double oldVx = vxPall;
			
			vxPall = vxPall - 2*vxPall*cosTheta*cosTheta - 2*vyPall*sinTheta*cosTheta; // Aggiorna velocità dopo l'urto coi piattelli
			vyPall = vyPall - 2*oldVx*sinTheta*cosTheta - 2*vyPall*sinTheta*sinTheta;
			
			//cout << "V: " << sqrt(vxPall*vxPall + vyPall*vyPall) << endl;
			toScatter = false;
			scoreAdd();
			
		}
	}
	if ( abs(xPall) < 0.1 ) toScatter = true;
	
	// Urto con soffitto e pavimento
	if ( abs(yPall) > 1 - r ) vyPall = -vyPall;
}
