GL: main.o gl.o grafica.o gioco.o
	g++ -Wall -pedantic-errors main.o gl.o grafica.o gioco.o -o GL -lGL -lm -lglut
	
main.o: main.cpp gl.h
	g++ -c main.cpp

gl.o: gl.cpp gl.h
	g++ -c gl.cpp
	
grafica.o: grafica.cpp gl.h
	g++ -c grafica.cpp
	
gioco.o: gioco.cpp gl.h
	g++ -c gioco.cpp

clean:
	rm -f *.o GL
