#ifndef GL_H
#define GL_H

#define PI 3.141592653

#define RECT_H 0.3
#define RECT_W 0.1

using namespace std;

// Rettangoli
extern double _left, _right;
extern int movingL, movingR;

// Pallina
extern double r, xPall, yPall, vxPall, vyPall;
extern bool toScatter;

// Simulazione
extern int elapsedTime, dt, maxFrameRate;
extern bool animating;

// Gioco
extern int score;

void timer(int);
void keyDown(unsigned char ch, int x, int y);
void keyUp(unsigned char ch, int x, int y);
void display();
void drawRectangle(double topLeftX, double topLeftY, double width, double height);
void updateRectangles(double left, double right);
void drawBall(double radius, double x, double y);
void scoreAdd();
void die();
void resetGame();
void updatePos();

#endif
